#include <Arduino.h>
#include <MIDIUSB.h>

// number of multiplexers
#define nMultiplexer 2
// number of channels per multiplexer
#define nChannelsMultiplexer 16
// difference of temporary stored and current value must greater than this to be recognized as a change
#define diffChange 2
//
#define nButtons 0

// multiplexer select pins
#define pinMultiplexSelect0 8
#define pinMultiplexSelect1 14
#define pinMultiplexSelect2 15
#define pinMultiplexSelect3 7

// data input pins
#define pinMultiplexDataPoti1 A1
#define pinMultiplexDataPoti2 A0
#define pinMultiplexDataButton 20

int pinInputs[2] = {pinMultiplexDataPoti1, pinMultiplexDataPoti2};
int valButton = 0;
int stateButton = 0;

void controlChange(byte channel, byte control, byte value)
{
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

// Save temporary control values in an array
int valStorage[nMultiplexer * nChannelsMultiplexer + 1];
int print = false;

void setup()
{
  if (print)
  {
    Serial.begin(115200);
  }
  // config pins
  pinMode(pinMultiplexDataPoti1, INPUT);
  pinMode(pinMultiplexDataPoti2, INPUT);
  pinMode(pinMultiplexDataButton, INPUT);

  pinMode(pinMultiplexSelect0, OUTPUT);
  pinMode(pinMultiplexSelect1, OUTPUT);
  pinMode(pinMultiplexSelect2, OUTPUT);
  pinMode(pinMultiplexSelect3, OUTPUT);

  // TODO inital read out and send to make vst and controller coherent
}

void loop()
{
  // iterate over all multiplex channels
  for (int i = 0; i <= nChannelsMultiplexer - 1; i++)
  {
    if (print)
    {
      Serial.print("channel ");
      Serial.println(i);
    }
    // set multiplexer byte
    digitalWrite(pinMultiplexSelect0, bitRead(i, 0));
    digitalWrite(pinMultiplexSelect1, bitRead(i, 1));
    digitalWrite(pinMultiplexSelect2, bitRead(i, 2));
    digitalWrite(pinMultiplexSelect3, bitRead(i, 3));

    //iterate over multiplexers
    for (int j = 0; j <= nMultiplexer - 1; j++)
    {

      // calculate current index
      int idx = j * nChannelsMultiplexer + i;
      // read incoming signal
      int val = analogRead(pinInputs[j]);
      // compare with value storage and send if changes
      int valOld = valStorage[idx];
      // check if difference is greater than required diff
      if (pow(val - valOld, 2) > diffChange)
      {
        controlChange(byte(0), byte(idx), (map(val, 1023, 0, 0, 128)));
        MidiUSB.flush();
        
      }

      // update old value
      valStorage[idx] = val;

      if (print)
      {
        Serial.print("idx ");
        Serial.print(idx);
        Serial.print("     in multiplexer ");
        Serial.print(j);
        Serial.print("    current vaL: ");
        Serial.print(val);
        Serial.print("   old vaL: ");
        Serial.print(valOld);
        Serial.println("");
      }
      delay(1);
    }
  }
  delay(1);
}