#include <Arduino.h>
#include <MIDIUSB.h>

// number of MIDI channels
#define nChannelsMidi 3

// number of multiplexers
#define nMultiplexer 2
// number of channels per multiplexer
#define nChannelsMultiplexer 16
// difference of temporary stored and current value must greater than this to be recognized as a change
#define diffChange 2
//
#define nButtons 16

// multiplexer select pins
#define pinMultiplexSelect0 8
#define pinMultiplexSelect1 14
#define pinMultiplexSelect2 15
#define pinMultiplexSelect3 7

// data input pins
#define pinMultiplexDataPoti1 A1
#define pinMultiplexDataPoti2 A0
#define pinMultiplexDataButton 3

int pinInputs[2] = {pinMultiplexDataPoti1, pinMultiplexDataPoti2};

void controlChange(byte channel, byte control, byte value)
{
  control=control+2;
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
  Serial.println(control);
        MidiUSB.flush();

}

// Save temporary control values in an array
int valStorage[nMultiplexer * nChannelsMultiplexer + 1];
int valButtonStorage[nButtons];
int stateButtonStorage[nButtons];
int print = false;

int channel = 0;

void setup()
{
  if (print)
  {
    Serial.begin(115200);
  }
  // config pins
  pinMode(pinMultiplexDataPoti1, INPUT);
  pinMode(pinMultiplexDataPoti2, INPUT);
  pinMode(pinMultiplexDataButton, INPUT_PULLUP);

  pinMode(pinMultiplexSelect0, OUTPUT);
  pinMode(pinMultiplexSelect1, OUTPUT);
  pinMode(pinMultiplexSelect2, OUTPUT);
  pinMode(pinMultiplexSelect3, OUTPUT);

  // TODO inital read out and send to make vst and controller coherent
}

void loop()
{
  // iterate over all multiplex channels
  for (int i = 0; i <= nChannelsMultiplexer - 1; i++)
  {
    if (print)
    {
      Serial.print("channel ");
      Serial.println(i);
    }
    // set multiplexer byte
    digitalWrite(pinMultiplexSelect0, bitRead(i, 0));
    digitalWrite(pinMultiplexSelect1, bitRead(i, 1));
    digitalWrite(pinMultiplexSelect2, bitRead(i, 2));
    digitalWrite(pinMultiplexSelect3, bitRead(i, 3));

    //iterate over multiplexers
    for (int j = 0; j <= nMultiplexer - 1; j++)
    {

      // calculate current index
      int idx = j * nChannelsMultiplexer + i;
      // read incoming signal
      int val = analogRead(pinInputs[j]);
      // compare with value storage and send if changes
      int valOld = valStorage[idx];
      // check if difference is greater than required diff
      if (pow(val - valOld, 2) > diffChange)
      {
        controlChange(byte(channel), byte(idx), (map(val, 1023, 0, 0, 127)));
      }

      // update old value
      valStorage[idx] = val;

      if (print)
      {
        Serial.print("idx ");
        Serial.print(idx);
        Serial.print("     in multiplexer ");
        Serial.print(j);
        Serial.print("    current vaL: ");
        Serial.print(val);
        Serial.print("   old vaL: ");
        Serial.print(valOld);
        Serial.println("");
      }
      delay(1);
    }
    // buttons

    /*
    n     function        states
    -------------------------------
    0     channel down      decrease
    1     osc mod           2 
    2     legato            3
    3     osc 3 K.T.        2
    4     noise             2
    5     feedback          2
    6     rel flt           2
    7     rel amp           2
    8     kt flt            2
    9     kt amp            2
    10    filter mod        2
    11    filter select     4
    12    contur pol        2
    13    channel up        increase
    14    -
    15    -

    */

    int nStates = 0;
    if (i < 14)
    {
      if (digitalRead(pinMultiplexDataButton) == 0)
      {
        switch (i)
        {
        case 0:
          if (channel >0)
          {
            channel--;
          }   
          else
          {
            channel = 15;
          }
          Serial.print("channel");
          Serial.println(channel);
          break;

        case 1:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          //Serial.println(map(stateButtonStorage[i], 0, 1, 0, 127));
          break;

        case 2:
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % 3;
          switch (stateButtonStorage[i])
          {
          case 0:
            controlChange(byte(channel), byte(i + 32), 0);
            break;
          case 1: 
            controlChange(byte(channel), byte(i + 32), 80);
            break;
          case 2:
            controlChange(byte(channel), byte(i + 32), 127);
            break;
          }
          break;

        case 3:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 4:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 5:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 7:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 8:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 9:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 10:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 125)));
          break;

        case 11:
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % 4;
          switch (stateButtonStorage[i])
          {
          case 0:
            controlChange(byte(channel), byte(i + 32), 0);
            break;
          case 1:
            controlChange(byte(channel), byte(i + 32), 50);
            break;
          case 2:
            controlChange(byte(channel), byte(i + 32), 105);
            break;
          case 3:
            controlChange(byte(channel), byte(i + 32), 127);
            break;
          }
          break;

        case 12:
          nStates = 2;
          stateButtonStorage[i] = (stateButtonStorage[i] + 1) % nStates;
          controlChange(byte(channel), byte(i + 32), (map(stateButtonStorage[i], 0, nStates - 1, 0, 127)));
          break;

        case 13:
          if (channel < nChannelsMidi)
          {
            channel++;
          }
          else
          {
            channel = 0;
          }
          Serial.print("channel");
          Serial.println(channel);
          break;

        default:
          break;
        }
      }

      MidiUSB.flush();
      while (digitalRead(pinMultiplexDataButton) == 0)
      {
        delay(1);
      }
    }
    delay(1);
  }
}